#-------------------------------------------------
#
# Project created by QtCreator 2014-11-13T14:42:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testParse
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    IsoFromUsbBuilder.cpp

HEADERS  += mainwindow.h \
    IsoFromUsbBuilder.h

FORMS    += mainwindow.ui
QMAKE_MAC_SDK = macosx10.9
