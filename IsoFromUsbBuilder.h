#ifndef ISOFROMUSBBUILDER_H
#define ISOFROMUSBBUILDER_H

#include <QObject>
#include <QProcess>
#include <QDebug>
#include <QFile>
#include <QString>
#include <QTimer>


class IsoFromUsbBuilder : public QObject
{
    Q_OBJECT
public:
    explicit IsoFromUsbBuilder(QString _pathToUsb, QString _isoName, QObject *parent = 0);

    void startBuildIso();

private:
    QFile*    isoFile;
    QProcess* ddProcess;
    QTimer*   checkPercentageTimer;
    QString   pathToUsb;
    QString   isoName;
    int       percent;

signals:
    void readyInPercentageSignal(int percentage);
    void startSignal();
    void finishSignal(bool okFlag);

public slots:
    void onStarProcessSlot();
    void onTimerTickSlot();
    void onFinishProcessSlot(int exitCode);

};

#endif // ISOFROMUSBBUILDER_H
