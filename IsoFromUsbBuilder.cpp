#include "IsoFromUsbBuilder.h"

// Инициализация
IsoFromUsbBuilder::IsoFromUsbBuilder(QString _pathToUsb, QString _isoName, QObject *parent) :
    QObject(parent) {

    pathToUsb = _pathToUsb;
    isoName   = _isoName;
    percent   = 4027580416 / 100;
}

// Запуск процесса снятия образа
// Привязка сигналов процесса
void IsoFromUsbBuilder::startBuildIso() {
    ddProcess = new QProcess(this);

    QStringList arguments;
    arguments.append("if=" + pathToUsb);
    arguments.append("of=" + isoName);

    connect(ddProcess, SIGNAL(started()), SLOT(onStarProcessSlot()));
    connect(ddProcess, SIGNAL(finished(int)), SLOT(onFinishProcessSlot(int)));

    ddProcess->setProcessChannelMode(QProcess::MergedChannels);
    ddProcess->start("dd", arguments);
}


// Реагирование на старт процесса
// Запуск файла
void IsoFromUsbBuilder::onStarProcessSlot() {

    isoFile = new QFile(isoName, this);

    checkPercentageTimer = new QTimer(this);
    connect(checkPercentageTimer, SIGNAL(timeout()), this, SLOT(onTimerTickSlot()));
    checkPercentageTimer->start(1000);
}


// Каждую секунду бросаем сигнал окончания в процентах
void IsoFromUsbBuilder::onTimerTickSlot() {

    emit readyInPercentageSignal(isoFile->size() / percent);
}


// По окончанию работы процесса, проверяю размер образа
// И кидаю отчет о записи
void IsoFromUsbBuilder::onFinishProcessSlot(int exitCode) {

    checkPercentageTimer->stop();
    emit finishSignal(isoFile->size() == 4027580416);
}
