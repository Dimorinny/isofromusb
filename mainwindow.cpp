#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    isoFromUsbBuilder = new IsoFromUsbBuilder("/dev/disk2", "/Users/Dimorinny/Desktop/lol.iso", this);
    ui->progressBar->setRange(0, 100);
    ui->progressBar->setValue(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setPercentage(int perc)
{
    ui->progressBar->setValue(perc);
}

void MainWindow::onFinishSlot(bool okFlag)
{
    if(!okFlag) {
        setPercentage(0);
        qDebug() << "err";
    } else {
        setPercentage(100);
    }
}

void MainWindow::on_pushButton_clicked()
{
    isoFromUsbBuilder->startBuildIso();
    connect(isoFromUsbBuilder, SIGNAL(readyInPercentageSignal(int)), SLOT(setPercentage(int)));
    connect(isoFromUsbBuilder, SIGNAL(finishSignal(bool)), SLOT(onFinishSlot(bool)));
}
