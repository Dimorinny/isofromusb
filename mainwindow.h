#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QProcess>
#include <QFile>
#include <QTimer>
#include <IsoFromUsbBuilder.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void setPercentage(int perc);
    void onFinishSlot(bool);
    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    IsoFromUsbBuilder* isoFromUsbBuilder;
    int percent;
};

#endif // MAINWINDOW_H
